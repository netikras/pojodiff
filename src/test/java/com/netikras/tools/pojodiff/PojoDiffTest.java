package com.netikras.tools.pojodiff;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.netikras.tools.pojodiff.ChangedValue.DiffType.*;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PojoDiffTest {

    @Test
    public void name() throws Exception {
        AbstractDiff<Person> diff = getDiff(john(), alice());
        diff.diff();
        System.out.println(diff);

        assertEquals(CHANGED, diff.getType());
        assertEquals(john(), diff.getOld());
        assertEquals(alice(), diff.getCurrent());

        ChangedValue<?> field = diff.getField("name");
        assertEquals(CHANGED, field.getType());
        assertEquals("John", field.getOld());
        assertEquals("Alice", field.getCurrent());

        field = diff.getField("eyes");
        assertEquals(NONE, field.getType());
        assertEquals("green", field.getOld());
        assertEquals("green", field.getCurrent());

        field = diff.getField("age");
        assertEquals(CHANGED, field.getType());
        assertEquals(23, field.getOld());
        assertEquals(33, field.getCurrent());

        field = diff.getField("languages");

        CollectionDiff<String, List<String>> languagesDiff = (CollectionDiff<String, List<String>>) field;
        List<ChangedValue<String>> languages = languagesDiff.getValues();
        assertEquals(3, languages.size());
        assertEquals(NONE, languages.get(0).getType());
        assertEquals("English", languages.get(0).getOld());
        assertEquals("English", languages.get(0).getCurrent());
        assertEquals(CHANGED, languages.get(1).getType());
        assertEquals("French", languages.get(1).getOld());
        assertEquals("Spanish", languages.get(1).getCurrent());
        assertEquals(REMOVED, languages.get(2).getType());
        assertEquals("Spanish", languages.get(2).getOld());
        assertEquals(null, languages.get(2).getCurrent());

        field = diff.getField("friends");
        CollectionDiff<Person, List<Person>> friendsDiff = (CollectionDiff<Person, List<Person>>) field;
        List<ChangedValue<Person>> friends = friendsDiff.getValues();
        assertEquals(2, friends.size());
        assertEquals(ADDED, friends.get(0).getType());
        assertEquals(null, friends.get(0).getOld());
        assertEquals(ADDED, friends.get(1).getType());
        assertEquals(null, friends.get(1).getOld());
        assertTrue(new HashSet<>(asList("Paula", "Jenny")).containsAll(asList(
                friends.get(0).getCurrent().getName(),
                friends.get(1).getCurrent().getName()
        )));

        //        System.out.println(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(diff));
    }

    private AbstractDiff<Person> getDiff(Person old, Person current) {
        return new PersonDiff(old, current);
    }

    public static class PersonDiff extends AbstractDiff<Person> {
        public PersonDiff(Person old, Person current) {
            super(old, current);
            addFieldAccessor("name", Person::getName)
                    .addFieldAccessor("age", Person::getAge)
                    .addFieldAccessor("eyes", Person::getEyeColor)
                    .addFieldAccessor("height", Person::getHeight)
                    .addFieldAccessor("spouse", Person::getSpouse, PersonDiff::new)
                    .addFieldAccessor("languages", Person::getSpokenLanguages,
                            (o, c) -> new CollectionDiff<>(o, c, AbstractDiff::new))
                    .addFieldAccessor("friends", p -> new ArrayList<>(p.getFriends()),
                            (o, c) -> new CollectionDiff<>(o, c, PersonDiff::new))
            ;
        }
    }

    private Person john() {
        Person object = new Person();
        object.setName("John");
        object.setAge(23);
        object.setEyeColor("green");
        object.setHeight(82.7);
        object.setSpouse(jenny());
        object.setSpokenLanguages(asList("English", "French", "Spanish"));
        return object;
    }

    private Person alice() {
        Person object = new Person();
        object.setName("Alice");
        object.setAge(33);
        object.setEyeColor("green");
        object.setHeight(79.6);
        object.setSpokenLanguages(asList("English", "Spanish"));
        object.setFriends(new HashSet<>(asList(jenny(), paula())));
        return object;
    }

    private Person jenny() {
        Person object = new Person();
        object.setName("Jenny");
        object.setAge(32);
        object.setEyeColor("blue");
        object.setHeight(81.3);
        object.setSpokenLanguages(asList("English", "Spanish", "Estonian", "Lithuanian", "English"));
        //        object.setFriends(Set.of(alice()));
        return object;
    }

    private Person paula() {
        Person object = new Person();
        object.setName("Paula");
        object.setAge(26);
        object.setEyeColor("brown");
        object.setHeight(77.6);
        object.setSpouse(john());
        object.setSpokenLanguages(asList("English"));
        //        object.setFriends(Set.of(alice()));
        return object;
    }

    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Person {
        private String name;
        private int age;
        private Double height;
        private String eyeColor;
        private Person spouse;
        private List<String> spokenLanguages;
        private Set<Person> friends;
    }
}