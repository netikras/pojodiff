package com.netikras.tools.pojodiff;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * This memento entity represents a change od a value. It contains an old as well as the new (current) values,
 * along with the nature of the change in the {@link #type} field.
 * @param <T> is the type of the changed value.
 */
@Getter
@Setter
@ToString
public class ChangedValue<T> {
    private DiffType type;
    private T old;
    private T current;

    public ChangedValue(DiffType type, T old, T current) {
        setType(type);
        setOld(old);
        setCurrent(current);
    }

    public ChangedValue() {
        setType(DiffType.NONE);
    }

    /**
     * Represents the nature of the value change.
     */
    public enum DiffType {
        /**
         * No differences between the values
         */
        NONE,

        /**
         * Value did not exist before, but it exists now
         */
        ADDED,

        /**
         * Value does not exist now, but it existed before
         */
        REMOVED,

        /**
         * Value was present before and is present now, but it has changed
         */
        CHANGED
    }
}
