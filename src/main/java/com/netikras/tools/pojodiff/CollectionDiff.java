package com.netikras.tools.pojodiff;

import lombok.*;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiFunction;

import static com.netikras.tools.pojodiff.ChangedValue.DiffType.*;

/**
 * This class is an enhanced version of {@link AbstractDiff}. While {@link AbstractDiff} can compare values and their
 * inner fields recursively, it cannot effectively deal with collections. This class, on the other hand,
 * focuses on collection diffs, to be more precise - on {@link AbstractDiff}s of their elements
 * <p/>
 * This entity itself represents a diff between collections. Usually collections differ by size,
 * so there's the field 'size'.
 * To access {@link AbstractDiff}s of collections' entries, refer to {@link #getElementsDiffs()} and {@link #getValues()}.
 * <p/>
 * To compare elements two helpers are required: {@link #elementsPicker} and {@link #elementDiffer}.
 * <p/>
 * {@link #elementsPicker} picks a single element from each collection. The default picker is
 * {@link IteratingElementsPicker} - it picks the next element of each collection every time,
 * and if the collection is out of elements - uses null instead.
 * <p/>
 * {@link #elementDiffer} compares two elements. Usually you may want to implement your own differ.
 *
 * @param <T> type of the elements inside collections
 * @param <C> type of the collection
 */
@Getter
@Setter
@ToString(callSuper = true)
public class CollectionDiff<T, C extends Collection<T>> extends AbstractDiff<C> {
    private final List<ChangedValue<T>> elementsDiffs;
    private final Differ<T> elementDiffer;
    private final BiFunction<C, C, ElementsPicker<T>> elementsPicker;

    public CollectionDiff(Callable<C> old, Callable<C> current, @NonNull Differ<T> elementDiffer) {
        this(old, current, elementDiffer, null);
    }

    public CollectionDiff(Callable<C> old, Callable<C> current, @NonNull Differ<T> elementDiffer,
            BiFunction<C, C, ElementsPicker<T>> pickers) {
        super(old, current);
        this.elementDiffer = elementDiffer;
        this.elementsDiffs = new ArrayList<>();

        if (getOld() == null) {
            setOld((C) new ArrayList<>());
        }
        if (getCurrent() == null) {
            setCurrent((C) new ArrayList<>());
        }

        this.elementsPicker = pickers != null ? pickers : IteratingElementsPicker::new;

        addFieldAccessor("size", Collection::size);
        addFieldAccessor("values", coll -> elementsDiffs);
    }

    public CollectionDiff(C old, C current, @NonNull Differ<T> innerValueDiff) {
        this(() -> old, () -> current, innerValueDiff);
    }

    public List<ChangedValue<T>> getValues() {
        return ((List<ChangedValue<T>>) Optional.ofNullable(getField("values")).map(v -> v.getCurrent()).orElse(null));
    }

    public ChangedValue<Integer> getSize() {
        return (ChangedValue<Integer>) getField("size");
    }

    @Override
    public boolean diff() {
        AtomicBoolean changed = new AtomicBoolean(super.diff());

        for (Elements<T> elements : elementsPicker.apply(getOld(), getCurrent())) {
            ChangedValue<T> diff = getElementDiffer().compare(elements.getOld(), elements.getCurrent());
            if (diff instanceof AbstractDiff) {
                AbstractDiff<?> abstractDiff = (AbstractDiff<?>) diff;
                if (abstractDiff.diff()) {
                    changed.set(true);
                    setType(CHANGED);
                }
            }
            getElementsDiffs().add(diff);
        }

        if (getOld().isEmpty() && getCurrent().isEmpty()) {
            setType(NONE);
            changed.set(true);
        } else if (getOld().isEmpty()) {
            setType(ADDED);
            changed.set(true);
        } else if (getCurrent().isEmpty()) {
            setType(REMOVED);
            changed.set(true);
        }

        //        ElementsPicker<T> picker = elementsPicker.apply(getOld(), getCurrent());
        //        for (Elements<T> elements : picker) {
        //            ChangedValue<T> diff = getElementDiffer().compare(elements.getOld(), elements.getCurrent());
        //            if (diff instanceof AbstractDiff abstractDiff) {
        //                if (abstractDiff.diff()) {
        //                    changed.set(true);
        //                    setType(CHANGED);
        //                }
        //            }
        //            getElementsDiffs().add(diff);
        //        }

        return changed.get();
    }

    public interface ElementsPicker<E> extends Iterator<Elements<E>>, Iterable<Elements<E>> {

        default Iterator<Elements<E>> iterator() {
            return this;
        }
    }


    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static
    class Elements<T> {
        private T old;
        private T current;
    }

    public static class IteratingElementsPicker<E> implements ElementsPicker<E> {
        private final Iterator<E> oldCollection;
        private final Iterator<E> currentCollection;

        public IteratingElementsPicker(Collection<E> oldCollection, Collection<E> currentCollection) {
            this(oldCollection.iterator(), currentCollection.iterator());
        }

        public IteratingElementsPicker(Iterator<E> oldCollection, Iterator<E> currentCollection) {
            this.oldCollection = oldCollection;
            this.currentCollection = currentCollection;
        }

        @Override
        public boolean hasNext() {
            return oldCollection.hasNext() || currentCollection.hasNext();
        }

        @Override public Elements<E> next() {
            return new Elements<>(
                    oldCollection.hasNext() ? oldCollection.next() : null,
                    currentCollection.hasNext() ? currentCollection.next() : null
            );
        }
    }

}
