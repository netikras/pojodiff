package com.netikras.tools.pojodiff;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.netikras.tools.pojodiff.ChangedValue.DiffType.*;
import static java.util.Arrays.asList;

/**
 * This class is an enhanced version of {@link ChangedValue}. While {@link ChangedValue} simply contains state, {@link AbstractDiff}
 * also contains
 * <p/>
 * <ul>
 *  <li>meta-state: differences of inner fields</li>
 *  <li>errors that occurred while accessing either value (should any occur, the value is treated as null)</li>
 *  <li>toolset, capable to diff two values (requires additional configuration to be provided: {@link #differs})</li>
 * </ul>
 *
 *
 * @param <T> see {@link ChangedValue}
 */
@Getter
@Setter
@ToString(callSuper = true)
public class AbstractDiff<T> extends ChangedValue<T> {
    private Throwable oldExc;
    private Throwable currExc;

    private final Map<String, @NonNull ChangedValue<?>> fieldsDiffs;
    private final Map<String, @NonNull Function<T, ?>> fieldAccessors;
    private final Map<String, @NonNull Differ> differs;

    private final Differ defaultDiffer = new SimpleDiffer<>();

    public AbstractDiff(@NonNull ChangedValue<T> diff) {
        this(diff.getOld(), diff.getCurrent());
        setType(diff.getType());

        if (diff instanceof AbstractDiff) {
            AbstractDiff<T> abstractDiff = (AbstractDiff<T>) diff;
            getFieldsDiffs().putAll(abstractDiff.getFieldsDiffs());
            getFieldAccessors().putAll(abstractDiff.getFieldAccessors());
            setOldExc(abstractDiff.getOldExc());
            setCurrExc(abstractDiff.getCurrExc());
        }
    }

    public AbstractDiff(Callable<T> old, Callable<T> current) {
        this();
        try {
            setOld(old.call());
        } catch (Exception e) {
            setOldExc(e);
        }
        try {
            setCurrent(current.call());
        } catch (Exception e) {
            setCurrExc(e);
        }
    }

    public AbstractDiff(T old, T current) {
        this();
        setOld(old);
        setCurrent(current);
    }

    public AbstractDiff() {
        setType(NONE);
        fieldAccessors = new HashMap<>();
        differs = new HashMap<>();
        fieldsDiffs = new HashMap<>();
    }

    public <T2> AbstractDiff<T> addFieldAccessor(@NonNull String fieldName, @NonNull Function<T, T2> accessor) {
        return addFieldAccessor(fieldName, accessor, new SimpleDiffer<>());
    }

    public <T2> AbstractDiff<T> addFieldAccessor(@NonNull String fieldName, @NonNull Function<T, T2> accessor, Differ<T2> diffMaker) {
        getFieldAccessors().put(fieldName, accessor);
        getDiffers().put(fieldName, diffMaker);
        return this;
    }

    public ChangedValue<?> getField(String name) {
        ChangedValue<?> changedValue = getFieldsDiffs().get(name);
        return changedValue;
    }

    public List<String> filterFields(Map<String, ChangedValue<?>> fields, DiffType... ofTypes) {
        Set<DiffType> types = new HashSet<>(asList(ofTypes));
        if (types.isEmpty()) {
            return new ArrayList<>();
        } else {
            return fields
                    .entrySet()
                    .stream()
                    .filter(e -> types.contains(e.getValue().getType()))
                    .map(Entry::getKey)
                    .collect(Collectors.toList())
                    ;
        }
    }

    public List<String> filterChangedFields() {
        return filterFields(getFieldsDiffs(), ADDED, REMOVED, CHANGED);
    }

    public boolean diff(T oldVersion, T currentVersion) {
        setOld(oldVersion);
        setCurrent(currentVersion);
        return diff();
    }

    public boolean diff() {
        AtomicBoolean changed = new AtomicBoolean(false);
        getFieldsDiffs().clear();

        if (getOld() == null && getCurrent() == null) {
//            compareFields();
            // nop
        } else if (getOld() == null) {
            compareFields();
            setType(ADDED);
            changed.set(true);
        } else if (getCurrent() == null) {
            compareFields();
            setType(REMOVED);
            changed.set(true);
        } else {
            if (getFieldAccessors().isEmpty()) {
                if (!Objects.equals(getOld(), getCurrent())) {
                    changed.set(true);
                    setType(CHANGED);
                }
            } else if (compareFields()) {
                changed.set(true);
                setType(CHANGED);
            }
        }

        return changed.get();
    }

    protected boolean compareFields() {
        AtomicBoolean changed = new AtomicBoolean(false);
        getFieldAccessors().forEach((name, accessor) -> {
            Differ<Object> differ = getDiffers().getOrDefault(name, defaultDiffer);
            Object old = access(getOld(), accessor);
            Object current = access(getCurrent(), accessor);
            ChangedValue<Object> diff = differ.compare(old, current);
            if (diff instanceof AbstractDiff) {
                AbstractDiff<?> abstractDiff = (AbstractDiff<?>) diff;
                if (abstractDiff.diff()) {
                    changed.set(true);
                }
            }
            if (!NONE.equals(diff.getType())) {
                changed.set(true);
            }
            getFieldsDiffs().put(name, diff);
        });

        return changed.get();
    }

    protected <V> V access(T object, Function<T, V> accessor) {
        try {
            return accessor.apply(object);
        } catch (Exception e) {
            return null;
        }
    }


    public interface Differ<D> {
        ChangedValue<D> compare(D old, D current);
    }

    public static class SimpleDiffer<D> implements Differ<D> {

        protected ChangedValue<D> toChangedValue(DiffType type, D old, D current) {
            return new ChangedValue<>(type, old, current);
        }

        @Override
        public ChangedValue<D> compare(D old, D current) {
            if (old == null && current == null) {
                return toChangedValue(NONE, old, current);
            } else if (old == null) {
                return toChangedValue(ADDED, old, current);
            } else if (current == null) {
                return toChangedValue(REMOVED, old, current);
            } else if (Objects.equals(old, current)) {
                return toChangedValue(NONE, old, current);
            } else {
                return toChangedValue(CHANGED, old, current);
            }
        }
    }
}
